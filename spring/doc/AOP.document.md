# 1. 概述
##1.1 AOP 的概念
 * AOP 并不是 Spring 框架的专属名称，它的全称是 Aspect Oriented Programming ，意为：面向切面编程。
 * 它是 OOP 的一个延续，通过预编译的方式和运行期间动态代理实现程序功能的统一维护的一种技术
 * 就是在程序运行某个方法的时候，不修改原始执行代码逻辑，由程序动态地执行某些额外的功能，对原有的方法做增强，这就叫做面向切面编程。
##1.2 AOP的名词解释
 * Aspect(切面):通常是一个类，里面可以定义切入点和通知
 * JointPoint(连接点):程序执行过程中明确的点，一般是方法的调用
 * Advice(通知):AOP在特定的切入点上执行的增强处理，有before,after,afterReturning,afterThrowing,around
 * Pointcut(切入点):就是带有通知的连接点，在程序中主要体现为书写切入点表达式
 * AOP代理：AOP框架创建的对象，代理就是对目标对象的加强。Spring中的AOP代理可以使JDK动态代理，也可以是CGLIB代理，前者基于接口，后者基于子类;
    由于cglib是基于继承来实现动态代理，所以无法对static、final修饰的类进行代理，也无法对static、private修饰的方法进行代理。