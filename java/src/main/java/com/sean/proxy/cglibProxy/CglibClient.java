package com.sean.proxy.cglibProxy;

import com.sean.proxy.common.RealTarget;
import com.sean.proxy.common.Subject;
import net.sf.cglib.proxy.Enhancer;

public class CglibClient {
    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(RealTarget.class);
        enhancer.setCallback(new CglibProxy());
        Subject subject= (Subject) enhancer.create();
        subject.request();
    }
}
