package com.sean.proxy.JdkProxy2;

import com.sean.proxy.common.Subject;

public class Client {
    public static void main(String[] args) {
        Subject subject = JdkProxyFactory.getSubject();
        subject.request();
    }
}
