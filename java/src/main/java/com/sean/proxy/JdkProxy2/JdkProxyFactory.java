package com.sean.proxy.JdkProxy2;

import com.sean.proxy.common.RealTarget;
import com.sean.proxy.common.Subject;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public  class JdkProxyFactory {

    public static Subject getSubject() {
        final Subject subject = new RealTarget();
        final MyAspect myAspect = new MyAspect();
        return (Subject) Proxy.newProxyInstance(JdkProxyFactory.class.getClassLoader(),
                new Class[]{Subject.class},
                new InvocationHandler() {
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        myAspect.myBefore();
                        Object invoke = method.invoke(subject, args);
                        myAspect.myAfter();
                        return invoke;
                    }
                });
    }

}
