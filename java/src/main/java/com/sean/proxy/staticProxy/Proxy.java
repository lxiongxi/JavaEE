package com.sean.proxy.staticProxy;

import com.sean.proxy.common.RealTarget;
import com.sean.proxy.common.Subject;

/**
 * <b>功能描述：</b>代理对象<br>
 * <b>修订记录：</b><br>
 * <li>20200814&nbsp;&nbsp;|&nbsp;&nbsp;刘雄喜&nbsp;&nbsp;|&nbsp;&nbsp;创建方法</li><br>
 *
 * @param:
 */
public class Proxy implements Subject {

    RealTarget realTarget;

    public Proxy(RealTarget realTarget) {
        this.realTarget = realTarget;
    }

    public void request() {
        System.out.println("before");
        realTarget.request();
        System.out.println("after");
    }
}
