package com.sean.proxy.staticProxy;

import com.sean.proxy.common.RealTarget;

/**
 * <b>功能：</b>客户端<br>
 * <b>Copyright ZJHY</b>
 * <ul>
 * <li>版本&nbsp;&nbsp;&nbsp;&nbsp;修改日期&nbsp;&nbsp;&nbsp;&nbsp;部　　门&nbsp;&nbsp;&nbsp;&nbsp;作　者&nbsp;&nbsp;&nbsp;&nbsp;变更内容</li>
 * <hr>
 * <li>v1.0&nbsp;&nbsp;&nbsp;&nbsp;20200814&nbsp;&nbsp;技术中心&nbsp;&nbsp;&nbsp;&nbsp;刘雄喜&nbsp;&nbsp;&nbsp;&nbsp;创建类</li>
 * </ul>
 */
public class Client {
    public static void main(String[] args) {
        Proxy proxy = new Proxy(new RealTarget());
        proxy.request();
    }
}
