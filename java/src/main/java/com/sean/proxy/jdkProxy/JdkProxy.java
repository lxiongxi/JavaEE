package com.sean.proxy.jdkProxy;


import com.sean.proxy.common.RealTarget;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * <b>功能：</b>JDk动态代理<br>
 * <b>Copyright ZJHY</b>
 * <ul>
 * <li>版本&nbsp;&nbsp;&nbsp;&nbsp;修改日期&nbsp;&nbsp;&nbsp;&nbsp;部　　门&nbsp;&nbsp;&nbsp;&nbsp;作　者&nbsp;&nbsp;&nbsp;&nbsp;变更内容</li>
 * <hr>
 * <li>v1.0&nbsp;&nbsp;&nbsp;&nbsp;20200814&nbsp;&nbsp;技术中心&nbsp;&nbsp;&nbsp;&nbsp;刘雄喜&nbsp;&nbsp;&nbsp;&nbsp;创建类</li>
 * </ul>
 */
public class JdkProxy implements InvocationHandler {
    //目标对象
    RealTarget realTarget;

    //强引用
    public JdkProxy(RealTarget realTarget) {
        this.realTarget = realTarget;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("before");
        //重点
        Object result = method.invoke(realTarget, args);
        System.out.println("after");
        return result;
    }
}
