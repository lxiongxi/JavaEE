package com.sean.proxy.jdkProxy;


import com.sean.proxy.common.RealTarget;
import com.sean.proxy.common.Subject;

import java.lang.reflect.Proxy;

public class Client {
    public static void main(String[] args) {
        Subject subject = (Subject) Proxy.newProxyInstance(Client.class.getClassLoader(),
                                                           new Class[]{Subject.class},
                                                           new JdkProxy(new RealTarget()));
        subject.request();
    }
}
