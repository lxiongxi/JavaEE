package com.sean.threadlocal;


import lombok.Data;

/**
 * ThreadLocal
 * 一、总结
 * 1.线程并发 : 在多线程并发的场景下
 * 2.传递数据 ：可以通过ThreadLocal在在同一线程 不同组件中传递公共变量
 * 3.线程隔离 ：每个线程都是相互隔离的 不会互相影响
 * 二、总结
 */

public class ThreadLocalTest {
    private String context;
    ThreadLocal<String> t1 = new ThreadLocal<>();

    public String getContext() {
        return t1.get();
    }

    public void setContext(String context) {
        t1.set(context);
    }

    public static void main(String[] args) {
        ThreadLocalTest threadLocalTest = new ThreadLocalTest();
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                threadLocalTest.setContext(Thread.currentThread().getName() + "的数据");
                System.out.println(Thread.currentThread().getName() + "\t------------->\t" + threadLocalTest.getContext());
            }, "线程" + i).start();
        }
    }
}
