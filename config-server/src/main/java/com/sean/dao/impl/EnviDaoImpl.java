package com.sean.dao.impl;


import com.sean.dao.EnviDao;
import com.sean.model.MongoEnvironment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;


import javax.annotation.Resource;


/**
 * <b>功能描述：</b>获取服务配置信息<br>
 * <b>修订记录：</b><br>
 * <li>20200821&nbsp;&nbsp;|&nbsp;&nbsp;刘雄喜&nbsp;&nbsp;|&nbsp;&nbsp;创建方法</li><br>
 */
@Repository
public class EnviDaoImpl implements EnviDao {

    @Resource
    private MongoTemplate mongoTemplate;

    @Value("${sup.mongodb.collection.name}")
    private String collectionName;

    private static String DEFAULT_PREFIX = "default";

    /**
     * <b>功能描述：</b>获取子服务的配置信息接口<br>
     * <b>修订记录：</b><br>
     * <li>20200821&nbsp;&nbsp;|&nbsp;&nbsp;刘雄喜&nbsp;&nbsp;|&nbsp;&nbsp;创建方法</li><br>
     *
     * @param application
     * @param profile
     * @param label
     */
    @Override
    public MongoEnvironment findServiceConfiguration(String application, String profile, String label) {
        Query query = new Query(
                Criteria.where("application").in(application, DEFAULT_PREFIX)
                        .and("profile").is(profile)
                        .and("label").in(label, DEFAULT_PREFIX));
        query.with(Sort.by(Sort.Direction.DESC, "level"));
        return mongoTemplate.findOne(query, MongoEnvironment.class, collectionName);

    }
}
