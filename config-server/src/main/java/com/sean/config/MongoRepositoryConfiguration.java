package com.sean.config;

import org.springframework.cloud.config.server.environment.EnvironmentRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * <b>功能：</b>注册MongoEnvironmentRepository Bean<br>
 * <b>Copyright ZJHY</b>
 * <ul>
 * <li>版本&nbsp;&nbsp;&nbsp;&nbsp;修改日期&nbsp;&nbsp;&nbsp;&nbsp;部　　门&nbsp;&nbsp;&nbsp;&nbsp;作　者&nbsp;&nbsp;&nbsp;&nbsp;变更内容</li>
 * <hr>
 * <li>v1.0&nbsp;&nbsp;&nbsp;&nbsp;20200817&nbsp;&nbsp;技术中心&nbsp;&nbsp;&nbsp;&nbsp;刘雄喜&nbsp;&nbsp;&nbsp;&nbsp;创建类</li>
 * </ul>
 */
@Configuration
public class MongoRepositoryConfiguration {


    @Bean
    public EnvironmentRepository openEnvironmentRepository() {
        return new MongoEnvironmentRepository();
    }
}
