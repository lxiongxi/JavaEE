package com.sean.config;


import com.sean.model.MongoEnvironment;
import com.sean.service.MongoRepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.config.environment.Environment;
import org.springframework.cloud.config.environment.PropertySource;
import org.springframework.cloud.config.server.environment.EnvironmentRepository;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;


/**
 * <b>功能：</b>获取子服务的配置信息方法<br>
 * <b>Copyright ZJHY</b>
 * <ul>
 * <li>版本&nbsp;&nbsp;&nbsp;&nbsp;修改日期&nbsp;&nbsp;&nbsp;&nbsp;部　　门&nbsp;&nbsp;&nbsp;&nbsp;作　者&nbsp;&nbsp;&nbsp;&nbsp;变更内容</li>
 * <hr>
 * <li>v1.0&nbsp;&nbsp;&nbsp;&nbsp;20200817&nbsp;&nbsp;技术中心&nbsp;&nbsp;&nbsp;&nbsp;刘雄喜&nbsp;&nbsp;&nbsp;&nbsp;创建类</li>
 * </ul>
 * application 应用名
 * profile 环境名称 （开发环境 prod dev grey）
 * label 分支名 （代码分支 master dev grey ）
 */
public class MongoEnvironmentRepository implements EnvironmentRepository {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private MongoRepositoryService mongoRepositoryService;

    @Override
    public Environment findOne(String application, String profile, String label) {
        if (StringUtils.isEmpty(application) || StringUtils.isEmpty(profile))
            return null;
        MongoEnvironment serviceConfiguration = mongoRepositoryService.findServiceConfiguration(application, profile, label);
        logger.info("服务配置文件 s%", serviceConfiguration);
        if (!StringUtils.isEmpty(serviceConfiguration)) {
            Environment environment = new Environment(application, StringUtils.commaDelimitedListToStringArray(profile),
                    serviceConfiguration.getLabel(), serviceConfiguration.getVersion(), null);
            environment.add(new PropertySource(serviceConfiguration.getLabel() + "/" + serviceConfiguration.getApplication() + "-" + serviceConfiguration.getProfile(), serviceConfiguration.getConf()));
            for (String profile_ : serviceConfiguration.getConfRef()) {
                serviceConfiguration = mongoRepositoryService.findServiceConfiguration(application, profile_, label);
                if (serviceConfiguration != null) {
                    environment.add(new PropertySource(serviceConfiguration.getLabel() + "/" + serviceConfiguration.getApplication() + "-" + serviceConfiguration.getProfile(), serviceConfiguration.getConf()));
                } else {
                    /** 加入日志 */
                    logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    logger.info("请到%s环境的mongodb中执行下面的语句：", label);
                    logger.info("db.getCollection('sys_envi').find({'application':'%s','profile':'%s','label':'%s'})", application, profile, label);
                    ;
                    logger.info("如果没有结果请执行下面的语句：");
                    logger.info(String.format("db.getCollection('sys_envi').find({'application':'%s','profile':'%s'})", application, profile, label));
                    logger.info("执行结果为出问题的服务。查看json中的conf-ref内的配置。");
                    logger.info("该服务缺少配置：" + profile_);
                    logger.info("执行下面语句查询，如果没有请加上。");
                    logger.info(String.format("db.getCollection('sys_envi').find({'profile':'%s'})", profile_));
                    logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                }
            }
            return environment;
        }
        return new Environment(application, profile);
    }
}



