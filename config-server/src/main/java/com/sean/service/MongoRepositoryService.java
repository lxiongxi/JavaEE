package com.sean.service;


import com.sean.dao.EnviDao;
import com.sean.model.MongoEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <b>功能：</b>获取子服务的配置信息服务<br>
 * <b>Copyright ZJHY</b>
 * <ul>
 * <li>版本&nbsp;&nbsp;&nbsp;&nbsp;修改日期&nbsp;&nbsp;&nbsp;&nbsp;部　　门&nbsp;&nbsp;&nbsp;&nbsp;作　者&nbsp;&nbsp;&nbsp;&nbsp;变更内容</li>
 * <hr>
 * <li>v1.0&nbsp;&nbsp;&nbsp;&nbsp;20200821&nbsp;&nbsp;技术中心&nbsp;&nbsp;&nbsp;&nbsp;刘雄喜&nbsp;&nbsp;&nbsp;&nbsp;创建类</li>
 * </ul>
 */
@Service
public class MongoRepositoryService {
    @Resource
    private EnviDao enviDao;
    

   public MongoEnvironment findServiceConfiguration(String application,String profile,String level){
       return enviDao.findServiceConfiguration(application, profile, level);
   }
}
